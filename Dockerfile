# For Java 8, try this
FROM openjdk:8

# Refer to Maven build -> finalName
ARG JAR_FILE=target/hazel-echo.jar

#EXPOSE 5701 5702
EXPOSE 5701 2552

COPY ${JAR_FILE} hazel-echo.jar
ENTRYPOINT ["java","-jar","hazel-echo.jar"]