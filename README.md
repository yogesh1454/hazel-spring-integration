# Grant access to Kubernetes API
Hazelcast uses Kubernetes API for the auto-discovery. 
That is why you need to grant certain roles to your service account. You can do it by creating the following “rbac.yaml” file.

# Then, apply it into your Kubernetes cluster.
=> kubectl apply -f rbac.yaml

# Deploy the application
Create “deployment.yaml” with Deployment and Service which will use the image you pushed to Docker Hub.
Then, to deploy an application, run the following command:
=> kubectl apply -f deployment.yaml

# Verify that the application works correctly
=> kubectl get all

# Verify Hazelcast member
In the logs for PODs, you should see that the Hazelcast members formed a cluster.
=> kubectl logs pod/{pod-name}
Members {size:2, ver:4} [
         Member [10.16.2.6]:5701 - 33076b61-e99d-46f2-b5c1-35e0e75f2311
         Member [10.16.2.8]:5701 - 9ba9bb61-6e34-460a-9208-c5a644490107 this
 ]