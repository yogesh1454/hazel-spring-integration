package org.visa.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HazelIntergrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(HazelIntergrationApplication.class, args);
	}

}
