package org.visa.payment.config.hazel;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IdGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class ServerNode {

    @Bean(name = "my-hazel-instance")
    public HazelcastInstance getInstance(){
        Config config =new Config();
/*
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getKubernetesConfig().setEnabled(true)
                .setProperty("namespace", "MY-KUBERNETES-NAMESPACE")
                .setProperty("service-name", "MY-SERVICE-NAME");
*/
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
        //Populate some data
        Map<Long, String> map = instance.getMap("data");
        IdGenerator idGenerator = instance.getIdGenerator("newid");
        for (int i = 0; i < 10; i++) {
            map.put(idGenerator.newId(), "message" + i);
        }
        return instance;
    }
}
