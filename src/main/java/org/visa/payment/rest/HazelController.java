package org.visa.payment.rest;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class HazelController {

    @Autowired
    @Qualifier("my-hazel-instance")
    private HazelcastInstance hazelcastInstance;

    HazelController(){
        super();
        environmentName =System.getenv("appEnv");
    }

    String environmentName;
    @PostMapping(value="put")
    public String putData(@RequestParam String value) throws UnknownHostException {
       String message;
        Map<Long,String> data =hazelcastInstance.getMap("data");
        IdGenerator idGenerator = hazelcastInstance.getIdGenerator("newid");

        message = InetAddress.getLocalHost().getHostName()
                +" @ "+environmentName
                +" @ "+(new Date())
                + value;
        System.out.println("Post data to hazel map: "+message);
        data.put(idGenerator.newId(), value);
        return message;
    }

    @GetMapping(value = "read")
    public Map<Long,String> readAllData() throws UnknownHostException {
        Map<Long,String> data = hazelcastInstance.getMap("data");
        String message = InetAddress.getLocalHost().getHostName()
                +" @ "+environmentName
                +" @ "+(new Date())
                + data;
        System.out.println("Post data to hazel map: "+message);
        return data;

    }

}
